//
//  secondVC.swift
//  StudentHomeWord02
//
//  Created by fred fu on 2020/9/3.
//  Copyright © 2020 fred fu. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {
    
    @IBOutlet weak var secondVCLable: UILabel!
    
    var str: String?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        secondVCLable.text = str
    }
    
    
    
    @IBAction func exitButtonClick(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}
