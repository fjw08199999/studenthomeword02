//
//  ViewController.swift
//  StudentHomeWord02
//
//  Created by fred fu on 2020/8/26.
//  Copyright © 2020 fred fu. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var list = [String]()
    

    @IBOutlet weak var tableView: UITableView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        list.append("user0")
        list.append("user1")
        list.append("user2")
        list.append("user3")
        list.append("user4")
        list.append("user5")
        list.append("user6")
        list.append("user7")
        list.append("user8")
        list.append("user9")
        
    }
    
    
   
    
    //表格三階段對話
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.textLabel?.text = list[indexPath.row]
        cell.imageView?.image = UIImage(named: list[indexPath.row])
        
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let str = list[indexPath.row]
//
//        print (str)
//    }
    
    //表格三階段對話
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let indexPath = tableView.indexPathForSelectedRow
        let str = list[indexPath!.row]
        let vc = segue.destination as? SecondVC
        vc?.str = str
        
    }
        
    
}

